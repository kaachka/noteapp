﻿using System;

namespace NotesApp.Application.Dto
{
    public class RoleDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}