﻿using System;

namespace NotesApp.Application.Dto
{
    public class RawNoteDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public string Text { get; set; }

        public DateTime? LifeTime { get; set; }

        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public UserDto User { get; set; }
    }
}