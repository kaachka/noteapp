﻿using System;
using System.Collections.Generic;

namespace NotesApp.Application.Dto
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }

        public RoleDto Role { get; set; }
        public List<NoteDto> Notes { get; set; }
    }
}