﻿using System.Collections.Generic;

namespace NotesApp.Application.Dto
{
    public class PaginationResultDto<TData>
    {
        public IEnumerable<TData> Data { get; set; }

        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
}