﻿using System;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Dto
{
    public class NoteDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public string Text { get; set; }

        public AccessType AccessType { get; set; }
        public DateTime? LifeTime { get; set; }

        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastModified { get; set; }
        public string? LastModifiedBy { get; set; }
        public UserDto User { get; set; }
    }
}