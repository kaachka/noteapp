﻿namespace NotesApp.Application.Dto
{
    public class PaginationDto
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}