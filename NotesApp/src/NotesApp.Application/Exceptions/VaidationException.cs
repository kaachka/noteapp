﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace NotesApp.Application.Exceptions
{
    public class ValidationException : Exception
    {
        public readonly Dictionary<string, string[]> Errors;

        public ValidationException()
        {
            Errors = new Dictionary<string, string[]>();
        }

        public ValidationException(ValidationFailure failure) : this()
        {
            Errors.Add(failure.PropertyName, new[] {failure.ErrorMessage});
        }
        
        public ValidationException(string message) : this(new ValidationFailure("Common", message))
        {
        }

        public ValidationException(IEnumerable<ValidationFailure> failures) : this()
        {
            Errors = failures.GroupBy(failure => failure.PropertyName, failure => failure.ErrorMessage)
                .ToDictionary(grouping => grouping.Key, grouping => grouping.ToArray());
        }
    }
}