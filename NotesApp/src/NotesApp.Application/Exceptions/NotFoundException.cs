﻿using System;

namespace NotesApp.Application.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException()
        {

        }

        public NotFoundException(string message) : base(message)
        {
            
        }

        public NotFoundException(string entity, object key, string keyName = null) : base($"Entity '{entity}' with {(string.IsNullOrWhiteSpace(keyName) ? "key" : keyName)} '{key}' not found.")
        {
            
        }
    }
}