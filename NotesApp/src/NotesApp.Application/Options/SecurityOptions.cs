﻿namespace NotesApp.Application.Options
{
    public class SecurityOptions
    {
        public string VerificationKeyApiPath { get; set; }
        public string VerificationKeyFilePath { get; set; }
    }
}