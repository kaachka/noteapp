﻿using System.Threading.Tasks;
using NotesApp.Application.Persistence.Repositories;

namespace NotesApp.Application.Persistence
{
    public interface IUnitOfWork
    {
        IRoleRepository RoleRepository { get; }
        IUserRepository UserRepository { get; }
        IRawNoteRepository RawNoteRepository { get; }
        INoteRepository NoteRepository { get; }

        Task<int> SaveChangesAsync();
    }
}