﻿using System;
using System.Threading.Tasks;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Persistence.Repositories
{
    public interface IRoleRepository : IRepository<Role, Guid>
    {
        Task<Role> GetAsync(string name);
    }
}