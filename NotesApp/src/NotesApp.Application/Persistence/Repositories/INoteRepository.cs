﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Persistence.Repositories
{
    public interface INoteRepository : IRepository<Note, Guid>
    {
        Task<bool> IsIdExists(Guid id);
        Task<IEnumerable<Note>> GetByAccessTypeAsync(Pagination pagination = null, AccessType accessType = AccessType.Public);
        Task<IEnumerable<Note>> GetExpiredAsync(Pagination pagination = null, AccessType accessType = AccessType.Public);
        Task<IEnumerable<Note>> FindAsync(Pagination pagination, string title);

    }
}