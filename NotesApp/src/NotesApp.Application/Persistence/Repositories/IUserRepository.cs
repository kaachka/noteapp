﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Persistence.Repositories
{
    public interface IUserRepository : IRepository<User, Guid>
    {
        Task<bool> IsEmailExistsAsync(string email);
        Task<bool> IsIdExistsAsync(Guid id);
        Task<User> GetAsync(Guid id, bool fullInfo = false);
        Task<IEnumerable<User>> GetAllAsync(Pagination pagination, bool fullInfo = false);
    }
}