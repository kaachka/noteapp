﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Persistence.Repositories
{
    public interface IRepository<TEntity, TId>
    {
        Task CreateAsync(TEntity entity);
        Task<TEntity> GetAsync(TId id);
        Task<IEnumerable<TEntity>> GetAllAsync(Pagination pagination);
        void Update(TEntity entity);
        Task RemoveAsync(TId id);
    }
}