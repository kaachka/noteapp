﻿using System;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Persistence.Repositories
{
    public interface IRawNoteRepository : IRepository<RawNote, Guid>
    {
        
    }
}