﻿using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Mapping
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, User>().ForMember(dest => dest.RoleId, options =>
                options.MapFrom(src => src.Id));
            CreateMap<User, UserDto>();
        }
    }
}