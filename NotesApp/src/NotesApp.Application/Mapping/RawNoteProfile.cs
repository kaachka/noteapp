﻿using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Mapping
{
    public class RawNoteProfile : Profile
    {
        public RawNoteProfile()
        {
            CreateMap<RawNote, RawNoteDto>().ReverseMap();
            CreateMap<RawNote, NoteDto>().ForPath(dest => dest.User.Id, options => options.MapFrom(src => src.UserId));
            
            CreateMap<RawNote, Note>();
        }
    }
}