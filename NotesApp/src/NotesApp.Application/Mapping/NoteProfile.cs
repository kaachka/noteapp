﻿using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Mapping
{
    public class NoteProfile : Profile
    {
        public NoteProfile()
        {
            CreateMap<Note, NoteDto>();
            CreateMap<NoteDto, Note>().ForMember(dest => dest.UserId, options => options.MapFrom(src => src.User.Id));

            CreateMap<NoteDto, RawNoteDto>();
        }
    }
}