﻿using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Mapping
{
    public class PaginationProfile : Profile
    {
        public PaginationProfile()
        {
            CreateMap<PaginationDto, Pagination>();

            CreateMap<PaginationResult<User>, PaginationResultDto<UserDto>>();
            CreateMap<PaginationResult<RawNote>, PaginationResultDto<RawNoteDto>>();
            CreateMap<PaginationResult<Note>, PaginationResultDto<NoteDto>>();
        }
    }
}