﻿using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Mapping
{
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<Role, RoleDto>().ReverseMap();
        }
    }
}