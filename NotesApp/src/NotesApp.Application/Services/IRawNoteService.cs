﻿using System;
using System.Threading.Tasks;
using NotesApp.Application.Dto;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Services
{
    public interface IRawNoteService
    {
        Task CreateAsync(RawNoteDto rawNoteDto);
        Task<RawNoteDto> GetAsync(Guid id);
        Task<RawNoteDto> GetAsync(string id);
        Task Admit(string id);
        Task Deny(string id);
        Task<PaginationResultDto<RawNoteDto>> GetAllAsync(Pagination pagination);
        Task UpdateAsync(RawNoteDto rawNoteDto);
        Task RemoveAsync(Guid id);
    }
}