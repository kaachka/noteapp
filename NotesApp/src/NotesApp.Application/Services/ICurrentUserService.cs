﻿using System.Threading.Tasks;

namespace NotesApp.Application.Services
{
    public interface ICurrentUserService
    {
        string? Id { get; set; }
        string? Nickname { get; set; }
        string? Role { get; set; }

        Task RefreshData(bool refreshRole = false);
    }
}