﻿using System;
using System.Threading.Tasks;
using NotesApp.Application.Dto;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Services
{
    public interface IUserService
    {
        Task CreateAsync(UserDto user);
        Task<UserDto> GetAsync(Guid id, bool fullInfo = false);
        Task<UserDto> GetAsync(string id, bool fullInfo = false);
        Task<PaginationResultDto<UserDto>> GetAll(Pagination pagination, bool fullInfo = false);
        Task<bool> IsEmailExists(string email);
        Task<bool> IsIdExists(Guid id);
        Task UpdateRole(string id, string roleName);
    }
}