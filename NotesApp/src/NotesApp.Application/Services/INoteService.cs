﻿using System;
using System.Threading.Tasks;
using NotesApp.Application.Dto;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Services
{
    public interface INoteService
    {
        Task CreateAsync(NoteDto noteDto);
        Task<NoteDto> GetAsync(Guid id);
        Task<NoteDto> GetAsync(string id);
        Task<PaginationResultDto<NoteDto>> GetAllAsync(Pagination pagination);

        Task<PaginationResultDto<NoteDto>> GetByAccessTypeAsync(Pagination pagination,
            AccessType accessType = AccessType.Public);

        Task<PaginationResultDto<NoteDto>> GetExpiredAsync(Pagination pagination,
            AccessType accessType = AccessType.Public);

        Task<PaginationResultDto<NoteDto>> FindAsync(Pagination pagination, string title);
        
        Task UpdateAsync(NoteDto noteDto);
        Task RemoveAsync(Guid id);
    }
}