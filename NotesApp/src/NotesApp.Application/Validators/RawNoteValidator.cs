﻿using FluentValidation;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Validators
{
    public class RawNoteValidator : AbstractValidator<RawNote>
    {
        public RawNoteValidator()
        {
            RuleFor(n => n.Title).NotEmpty().Length(3, 50);
            RuleFor(n => n.Description).MaximumLength(80);
            RuleFor(n => n.Text).NotEmpty();
            
            RuleFor(n => n.CreatedBy).Length(3, 128);
            RuleFor(n => n.LastModifiedBy).Length(3, 128);
        }
    }
}