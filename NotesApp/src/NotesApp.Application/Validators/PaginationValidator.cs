﻿using FluentValidation;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Validators
{
    public class PaginationValidator : AbstractValidator<Pagination>
    {
        public PaginationValidator()
        {
            RuleFor(p => p.PageNumber).GreaterThan(0);
            RuleFor(p => p.PageSize).InclusiveBetween(1, 48);
        }
    }
}