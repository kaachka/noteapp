﻿using FluentValidation;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Validators
{
    public class NoteValidator : AbstractValidator<Note>
    {
        public NoteValidator()
        {
            RuleFor(n => n.Title).NotEmpty().Length(3, 80);
            RuleFor(n => n.Description).MaximumLength(128);
            RuleFor(n => n.Text).NotEmpty();
            RuleFor(n => n.AccessType).NotNull();
            // RuleFor(n => n.UserId).NotEmpty();
            
            // RuleFor(n => n.Created).NotEmpty();
            // RuleFor(n => n.CreatedBy).NotEmpty().Length(3, 128);
            RuleFor(n => n.LastModifiedBy).Length(3, 128);
        }
    }
}