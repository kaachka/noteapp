﻿using FluentValidation;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Validators
{
    public class RoleValidator : AbstractValidator<Role>
    {
        public RoleValidator()
        {
            RuleFor(r => r.Name).NotEmpty().Length(3, 80);
        }
    }
}