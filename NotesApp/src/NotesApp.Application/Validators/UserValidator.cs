﻿using FluentValidation;
using NotesApp.Domain.Entities;

namespace NotesApp.Application.Validators
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(u => u.FirstName).NotEmpty().Length(3, 48);
            RuleFor(u => u.LastName).NotEmpty().Length(3, 48);
            RuleFor(u => u.Nickname).NotEmpty().Length(3, 32);
            RuleFor(u => u.Email).NotEmpty().Length(10, 128); //match
        }
    }
}