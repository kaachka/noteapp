﻿using System;

namespace NotesApp.WebUI.Responses.Note
{
    public class NoteResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public string Text { get; set; }
        public string AccessType { get; set; }
        public DateTime Created { get; set; }
        public DateTime? LifeTime { get; set; }
        public Guid? UserId { get; set; }
        public string Nickname { get; set; }
    }
}