﻿using System;

namespace NotesApp.WebUI.Responses.RawNote
{
    public class RawNoteResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }

        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public Guid? UserId { get; set; }
    }
}