﻿using System.Collections.Generic;

namespace NotesApp.WebUI.Responses
{
    public class PaginationResponse<TData>
    {
        public IEnumerable<TData> Data { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}