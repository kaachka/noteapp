﻿using System;
using System.Collections.Generic;
using NotesApp.WebUI.Responses.Note;

namespace NotesApp.WebUI.Responses.User
{
    public class UserResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public IEnumerable<NoteResponse> Notes { get; set; }
    }
}