﻿using System;

namespace NotesApp.WebUI.Requests.Note
{
    public class UpdateNoteRequest : CommonNoteRequest
    {
        public Guid Id { get; set; }
    }
}