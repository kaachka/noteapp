﻿using System;
using NotesApp.Domain.Entities;

namespace NotesApp.WebUI.Requests.Note
{
    public class CreateNoteRequest2
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public AccessType AccessType { get; set; }
        public DateTime? LifeTime { get; set; }
    }
}