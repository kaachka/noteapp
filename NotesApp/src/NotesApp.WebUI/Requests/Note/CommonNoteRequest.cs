﻿using System;
using NotesApp.Domain.Entities;

namespace NotesApp.WebUI.Requests.Note
{
    public abstract class CommonNoteRequest
    {
        public string Title { get; set; }
        // public string? Description { get; set; }
        public string Text { get; set; }
        public AccessType AccessType { get; set; }
        public DateTime? LifeTime { get; set; }
    }
}