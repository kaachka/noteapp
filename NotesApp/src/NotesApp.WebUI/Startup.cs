using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using NotesApp.Application;
using NotesApp.Infrastructure;
using NotesApp.WebUI.Filters;
using NotesApp.WebUI.Middlewares;

namespace NotesApp.WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();

            services.AddApplication();
            services.AddInfrastructure(Configuration);

            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            // services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();

            services.AddControllers(
                //     .AddJsonOptions(options =>
                // {
                //     options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
                // });
                options =>
                {
                    // options.Filters.Add<ExceptionFilter>();
                });

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "NotesApp.WebUI", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "NotesApp.WebUI v1"));
            }

            app.UseHttpsRedirection();
          
            app.UseRouting();

            app.UseCors();
            
            app.UseAuthentication();
            app.UseRoles();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}