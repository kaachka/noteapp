﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NotesApp.Application.Services;
using NotesApp.Domain.Entities;
using NotesApp.WebUI.Requests;
using NotesApp.WebUI.Responses;
using NotesApp.WebUI.Responses.RawNote;

namespace NotesApp.WebUI.Controllers
{
    // [Authorize(Roles = "Editor, Admin")]
    [Route("api/v1/raw-notes")]
    public class RawNoteController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRawNoteService _rawNoteService;

        public RawNoteController(IMapper mapper,IRawNoteService rawNoteService)
        {
            _mapper = mapper;
            _rawNoteService = rawNoteService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            return Ok(await _rawNoteService.GetAsync(id));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] PaginationRequest request)
        {
            var data = await _rawNoteService.GetAllAsync(_mapper.Map<Pagination>(request)); 
            
            return Ok(_mapper.Map<PaginationResponse<RawNoteResponse>>(data));
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> AdmitNote([FromRoute] string id)
        {
            await _rawNoteService.Admit(id);
            return Ok();
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DenyNote([FromRoute] string id)
        {
            await _rawNoteService.Deny(id);
            return Ok();
        }
    }
}