﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NotesApp.Application.Dto;
using NotesApp.Application.Services;
using NotesApp.Domain.Entities;
using NotesApp.WebUI.Requests;
using NotesApp.WebUI.Requests.Note;
using NotesApp.WebUI.Responses;
using NotesApp.WebUI.Responses.Note;

namespace NotesApp.WebUI.Controllers
{
    [Authorize]
    [Route("api/v1/notes")]
    // [ProducesResponseType(StatusCodes.Status200OK)]
    // [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public class NoteController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRawNoteService _rawNoteService;
        private readonly INoteService _noteService;

        public NoteController(IMapper mapper, IRawNoteService rawNoteService, INoteService noteService)
        {
            _mapper = mapper;
            _rawNoteService = rawNoteService;
            _noteService = noteService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Create([FromBody] CreateNoteRequest request)
        {
            await _noteService.CreateAsync(_mapper.Map<NoteDto>(request));
            return Ok();
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var data = _mapper.Map<NoteResponse>(await _noteService.GetAsync(id));
            return Ok(data);
        }

        [Route("find")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Find([FromQuery] PaginationRequest request, [FromQuery] string title)
        {
            var data = await _noteService.FindAsync(_mapper.Map<Pagination>(request), title);

            return Ok(_mapper.Map<PaginationResponse<NoteResponse>>(data));
        }
        
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll([FromQuery] PaginationRequest request)
        {
            var data = await _noteService.GetAllAsync(_mapper.Map<Pagination>(request));
            return Ok(_mapper.Map<PaginationResponse<NoteResponse>>(data));
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromRoute] string id, [FromBody] UpdateNoteRequest request)
        {
            request.Id = new Guid(id);
            await _noteService.UpdateAsync(_mapper.Map<NoteDto>(request));
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove([FromRoute] string id)
        {
            await _noteService.RemoveAsync(new Guid(id));
            return Ok();
        }
    }
}