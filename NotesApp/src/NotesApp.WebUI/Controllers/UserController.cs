﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NotesApp.Application.Services;
using NotesApp.Domain.Entities;
using NotesApp.WebUI.Requests;
using NotesApp.WebUI.Responses;
using NotesApp.WebUI.Responses.User;

namespace NotesApp.WebUI.Controllers
{
    [Authorize]
    [Route("api/v1/users")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromRoute] string id, [FromQuery] bool fullInfo = false)
        {
            var userDTO = await _userService.GetAsync(id, fullInfo);
            
            return Ok(_mapper.Map<UserResponse>(userDTO));
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Get([FromQuery] PaginationRequest paginationRequest, [FromQuery] bool fullInfo = false)
        {
            var users = await _userService.GetAll(_mapper.Map<Pagination>(paginationRequest), fullInfo);

            return Ok(_mapper.Map<PaginationResponse<UserResponse>>(users));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Update([FromRoute] string id, [FromBody] string role)
        {
            await _userService.UpdateRole(id, role);
            
            return Ok();
        }
    }
}