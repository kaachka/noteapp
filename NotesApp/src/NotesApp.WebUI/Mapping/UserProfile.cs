﻿using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.WebUI.Responses;
using NotesApp.WebUI.Responses.User;

namespace NotesApp.WebUI.Mapping
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, UserResponse>()
                .ForMember(dest => dest.Role, option => option.MapFrom(src => src.Role.Name));
            
            CreateMap<PaginationResultDto<UserDto>, PaginationResponse<UserResponse>>();
        }
    }
}