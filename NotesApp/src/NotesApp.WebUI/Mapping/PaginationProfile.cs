﻿using AutoMapper;
using NotesApp.Domain.Entities;
using NotesApp.WebUI.Requests;

namespace NotesApp.WebUI.Mapping
{
    public class PaginationProfile : Profile
    {
        public PaginationProfile()
        {
            CreateMap<PaginationRequest, Pagination>();  
        }
    }
}