﻿using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.WebUI.Requests.Note;
using NotesApp.WebUI.Responses;
using NotesApp.WebUI.Responses.Note;

namespace NotesApp.WebUI.Mapping
{
    public class NoteProfile : Profile
    {
        public NoteProfile()
        {
            CreateMap<CreateNoteRequest, NoteDto>();
            CreateMap<UpdateNoteRequest, NoteDto>();
            
            CreateMap<NoteDto, NoteResponse>().
                ForMember(dest => dest.UserId, options => 
                options.MapFrom(src => src.User.Id)).
                ForMember(dest => dest.Nickname, option =>
                    option.MapFrom(src => src.CreatedBy));

            CreateMap<PaginationResultDto<NoteDto>, PaginationResponse<NoteResponse>>();
        }
    }
}