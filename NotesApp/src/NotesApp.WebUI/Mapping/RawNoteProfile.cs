﻿using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.WebUI.Requests.Note;
using NotesApp.WebUI.Responses;
using NotesApp.WebUI.Responses.RawNote;

namespace NotesApp.WebUI.Mapping
{
    public class RawNoteProfile : Profile
    {
        public RawNoteProfile()
        {
            CreateMap<CreateNoteRequest, RawNoteDto>();
            
            CreateMap<RawNoteDto, RawNoteResponse>().ForMember(dest => dest.UserId, options =>
                options.MapFrom(src => src.User.Id));
            
            CreateMap<PaginationResultDto<RawNoteDto>, PaginationResponse<RawNoteResponse>>();
        }
    }
}