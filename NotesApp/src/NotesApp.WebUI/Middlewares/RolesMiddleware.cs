﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NotesApp.Application.Services;

namespace NotesApp.WebUI.Middlewares
{
    public class RolesMiddleware
    {
        private readonly RequestDelegate _next;

        public RolesMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ICurrentUserService currentUserService)
        {
            await currentUserService.RefreshData();

            await _next(context);
        }
    }
}