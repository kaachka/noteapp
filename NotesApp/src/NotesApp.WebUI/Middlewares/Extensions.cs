﻿using Microsoft.AspNetCore.Builder;

namespace NotesApp.WebUI.Middlewares
{
    public static class Extensions
    {
        public static IApplicationBuilder UseRoles(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<RolesMiddleware>();
            return builder;
        }
    }
}