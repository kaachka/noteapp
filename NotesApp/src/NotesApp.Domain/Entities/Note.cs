﻿using System;

namespace NotesApp.Domain.Entities
{
    public class Note : AuditableEntity
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public string Text { get; set; }

        public AccessType AccessType { get; set; } = AccessType.Public;
        public DateTime? LifeTime { get; set; } = DateTime.UtcNow.AddHours(24);

        public Guid? UserId { get; set; }
        public User? User { get; set; }
    }
}