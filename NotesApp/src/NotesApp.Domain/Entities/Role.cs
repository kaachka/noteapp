﻿using System;
using System.Collections.Generic;

namespace NotesApp.Domain.Entities
{
    public class Role
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public List<User> Users { get; } = new();
    }
}