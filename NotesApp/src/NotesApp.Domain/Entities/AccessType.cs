﻿namespace NotesApp.Domain.Entities
{
    public enum AccessType
    {
        Private = 0,
        Link = 1,
        Public = 2
    }
}