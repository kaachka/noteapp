﻿using System.Collections.Generic;

namespace NotesApp.Domain.Entities
{
    public class PaginationResult<TData>
    {
        public IEnumerable<TData> Data { get; set; }

        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        
        public PaginationResult() { }

        public PaginationResult(IEnumerable<TData> data)
        {
            Data = data;
        }
    }
}