﻿using System;

namespace NotesApp.Domain.Entities
{
    public class RawNote : AuditableEntity
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public string Text { get; set; }

        public DateTime? LifeTime { get; set; } = DateTime.UtcNow.AddHours(24);

        public Guid? UserId { get; set; }
        public User? User { get; set; }
    }
}