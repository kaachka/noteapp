﻿namespace NotesApp.Domain.Entities
{
    /// <remarks>
    /// Default values:<br/>
    /// PageNumber - 1,<br/>
    /// PageSize - 16.
    /// </remarks>
    public class Pagination
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 16;
    }
}