﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NotesApp.Application.Dto;
using NotesApp.Application.Exceptions;
using NotesApp.Application.Options;
using NotesApp.Application.Persistence;
using NotesApp.Application.Services;
using NotesApp.Infrastructure.Persistence;
using NotesApp.Infrastructure.Persistence.Database;
using NotesApp.Infrastructure.Services;

namespace NotesApp.Infrastructure
{
    public static class DependencyInjection
    {
        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            var securityOptions = new SecurityOptions();
            var authOptions = new AuthOptions();
            var notesOptions = new NotesOptions();

            configuration.Bind(nameof(SecurityOptions), securityOptions);
            configuration.Bind(nameof(AuthOptions), authOptions);
            configuration.Bind(nameof(NotesOptions), notesOptions);

            services.AddSingleton(securityOptions);
            services.AddSingleton(authOptions);
            services.AddSingleton(notesOptions);

            services.AddCommonServices();

            services.AddApplicationDbContext(configuration);
            //services.AddHttpContextAccessor(); in Startup

            services.AddApplicationAuthentication(securityOptions, authOptions);
        }

        private static void AddCommonServices(this IServiceCollection services)
        {
            // services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddSingleton<ICurrentUserService, CurrentUserService>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRawNoteService, RawNoteService>();
            services.AddScoped<INoteService, NoteService>();
        }

        private static void AddApplicationDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(contextOptions =>
                contextOptions.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                    options => options.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));
        }

        private static void AddApplicationAuthentication(this IServiceCollection services,
            SecurityOptions securityOptions, AuthOptions authOptions)
        {
            UpdateVerificationKey(securityOptions);

            JwtSecurityTokenHandler.DefaultMapInboundClaims = false;

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = true;

                var rsa = System.Security.Cryptography.RSA.Create();

                rsa.ImportFromPem(File.ReadAllText(securityOptions.VerificationKeyFilePath).ToCharArray());
                // rsa.FromXmlString(File.ReadAllText(securityOptions.VerificationKeyFilePath));

                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = _ => Task.CompletedTask,
                    OnAuthenticationFailed = _ => Task.CompletedTask,
                    OnTokenValidated = async context =>
                    {
                        var token = context.SecurityToken as JwtSecurityToken;
                            
                        try
                        {
                            var userService =
                                context.HttpContext.RequestServices.GetRequiredService<IUserService>();

                            var email = token!.Claims.First(c => c.Type == JwtRegisteredClaimNames.Email).Value;

                            if (await userService.IsEmailExists(email))
                                return;

                            var id = token.Claims.First(c => c.Type == JwtRegisteredClaimNames.Sub).Value;
                            var firstName = token.Claims.First(c => c.Type == JwtRegisteredClaimNames.GivenName)
                                .Value;
                            var lastName = token.Claims.First(c => c.Type == JwtRegisteredClaimNames.FamilyName)
                                .Value;
                            var nickName = token.Claims.First(c => c.Type == JwtRegisteredClaimNames.UniqueName)
                                .Value;

                            var newUser = new UserDto
                            {
                                Id = new Guid(id),
                                FirstName = firstName,
                                LastName = lastName,
                                Email = email,
                                Nickname = nickName
                            };

                            await userService.CreateAsync(newUser);
                        }
                        catch
                        {
                            // ignored
                        }
                    }
                };

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,

                    ClockSkew = TimeSpan.Zero,

                    ValidIssuer = authOptions.Issuer,
                    ValidAudience = authOptions.Audience,
                    IssuerSigningKey = new RsaSecurityKey(rsa)
                };
            });
        }

        private static void UpdateVerificationKey(SecurityOptions securityOptions)
        {
            var keyFolder = Path.GetDirectoryName(securityOptions.VerificationKeyFilePath);

            if (!Directory.Exists(keyFolder))
                Directory.CreateDirectory(keyFolder);

            var response = new HttpClient().GetAsync(securityOptions.VerificationKeyApiPath).ConfigureAwait(false)
                .GetAwaiter().GetResult();

            response.EnsureSuccessStatusCode();

            var verificationKey =
                response.Content.ReadAsStringAsync().ConfigureAwait(false).GetAwaiter().GetResult();

            if (File.Exists(securityOptions.VerificationKeyFilePath))
            {
                var localVerificationKey = File.ReadAllText(securityOptions.VerificationKeyFilePath);

                if (localVerificationKey == verificationKey)
                    return;
            }

            using var keyFile = File.Create(securityOptions.VerificationKeyFilePath);
            keyFile.Write(Encoding.UTF8.GetBytes(verificationKey));
            keyFile.Dispose();
        }
    }
}