﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.Application.Exceptions;
using NotesApp.Application.Persistence;
using NotesApp.Application.Services;
using NotesApp.Application.Validators;
using NotesApp.Domain.Entities;

namespace NotesApp.Infrastructure.Services
{
    public class RawNoteService : IRawNoteService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        
        private readonly RawNoteValidator _rawNoteValidator;
        private readonly PaginationValidator _paginationValidator;

        public RawNoteService(IMapper mapper, IUnitOfWork unitOfWork, RawNoteValidator rawNoteValidator, PaginationValidator paginationValidator)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            
            _rawNoteValidator = rawNoteValidator;
            _paginationValidator = paginationValidator;
        }

        private Guid ParseGuidString(string id)
        {
            try
            {
                return new Guid(id);
            }
            catch
            {
                throw new ValidationException("Incorrect id type.");
            }
        }
        
        private async Task ValidateAsync(RawNote rawNote)
        {
            var result = await _rawNoteValidator.ValidateAsync(rawNote);

            if (!result.IsValid)
                throw new ValidationException(result.Errors);
        }
        
        private async Task<Pagination> ConfirmPagination(Pagination pagination)
        {
            var result = await _paginationValidator.ValidateAsync(pagination);

            return result.IsValid ? pagination : new Pagination();
        }
        
        public async Task CreateAsync(RawNoteDto rawNoteDto)
        {
            var rawNote = _mapper.Map<RawNote>(rawNoteDto);

            await ValidateAsync(rawNote);

            if (rawNote.User is not null)
            {
                rawNote.UserId = rawNote.User.Id;
                rawNote.User = null;
            }

            await _unitOfWork.RawNoteRepository.CreateAsync(rawNote);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<RawNoteDto> GetAsync(Guid id)
        {
            return _mapper.Map<RawNoteDto>(await _unitOfWork.RawNoteRepository.GetAsync(id));
        }

        public async Task<RawNoteDto> GetAsync(string id)
        {
            var noteId = ParseGuidString(id);
            
            return _mapper.Map<RawNoteDto>(await _unitOfWork.RawNoteRepository.GetAsync(noteId));
        }

        public async Task Admit(string id)
        {
            var noteId = ParseGuidString(id);
            var rawNote = await _unitOfWork.RawNoteRepository.GetAsync(noteId);
            var note = _mapper.Map<Note>(rawNote);
            
            if (note.UserId is null)
            {
                await _unitOfWork.NoteRepository.CreateAsync(note);
                await _unitOfWork.RawNoteRepository.RemoveAsync(noteId);
                await _unitOfWork.SaveChangesAsync();
                return;
            }
            
            if (rawNote.LifeTime is not null)
            {
                note.LifeTime = DateTime.UtcNow.Add(rawNote.LifeTime.Value.Subtract(rawNote.Created));
            }

            await _unitOfWork.NoteRepository.CreateAsync(note);
            await _unitOfWork.RawNoteRepository.RemoveAsync(noteId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task Deny(string id)
        {
            var noteId = ParseGuidString(id);
            await RemoveAsync(noteId);
        }

        public async Task<PaginationResultDto<RawNoteDto>> GetAllAsync(Pagination pagination)
        {
            var confirmedPagination = await ConfirmPagination(pagination);

            var data = await _unitOfWork.RawNoteRepository.GetAllAsync(confirmedPagination);
            
            return new PaginationResultDto<RawNoteDto>
            {
                PageNumber = confirmedPagination.PageNumber,
                PageSize = confirmedPagination.PageSize,
                Data = _mapper.Map<IEnumerable<RawNoteDto>>(data)
            };
        }

        public async Task UpdateAsync(RawNoteDto rawNoteDto)
        {
            var rawNote = _mapper.Map<RawNote>(rawNoteDto);

            await ValidateAsync(rawNote);
           
            _unitOfWork.RawNoteRepository.Update(rawNote);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            await _unitOfWork.RawNoteRepository.RemoveAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}