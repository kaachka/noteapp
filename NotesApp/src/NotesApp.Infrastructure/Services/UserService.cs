﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Results;
using NotesApp.Application.Dto;
using NotesApp.Application.Exceptions;
using NotesApp.Application.Persistence;
using NotesApp.Application.Services;
using NotesApp.Application.Validators;
using NotesApp.Domain.Entities;

namespace NotesApp.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        private readonly UserValidator _userValidator;
        private readonly PaginationValidator _paginationValidator;

        public UserService(IMapper mapper, IUnitOfWork unitOfWork, UserValidator userValidator, PaginationValidator paginationValidator)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;

            _userValidator = userValidator;
            _paginationValidator = paginationValidator;
        }

        private Guid ParseGuidString(string id)
        {
            Guid parsedId;
            
            try
            {
                parsedId = new Guid(id);
            }
            catch
            {
                throw new ValidationException("Incorrect id type.");
            }

            return parsedId;
        }
        
        private async Task ValidateAsync(User entity)
        {
            var result = await _userValidator.ValidateAsync(entity);

            if (!result.IsValid)
                throw new ValidationException(result.Errors);
        }
        
        private async Task<Pagination> ConfirmPagination(Pagination pagination)
        {
            var result = await _paginationValidator.ValidateAsync(pagination);

            return result.IsValid ? pagination : new Pagination();
        }
        
        public async Task CreateAsync(UserDto user)
        {
            if (await _unitOfWork.UserRepository.IsEmailExistsAsync(user.Email))
                throw new ValidationException(new ValidationFailure(nameof(user.Email), $"Email '{user.Email}' already used."));

            var entity = _mapper.Map<User>(user);
            
            await ValidateAsync(entity);
            
            entity.Role ??= await _unitOfWork.RoleRepository.GetAsync("User");

            await _unitOfWork.UserRepository.CreateAsync(entity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<UserDto> GetAsync(Guid id, bool fullInfo = false)
        {
            return _mapper.Map<UserDto>(await _unitOfWork.UserRepository.GetAsync(id, fullInfo));
        }
        
        public async Task<UserDto> GetAsync(string id, bool fullInfo = false)
        {
            return await GetAsync(ParseGuidString(id), fullInfo);
        }

        public async Task<PaginationResultDto<UserDto>> GetAll(Pagination pagination, bool fullInfo = false)
        {
            var confirmedPagination = await ConfirmPagination(pagination);

            var data = await _unitOfWork.UserRepository.GetAllAsync(confirmedPagination, fullInfo);

            return new PaginationResultDto<UserDto>
            {
                PageNumber = confirmedPagination.PageNumber,
                PageSize = confirmedPagination.PageSize,
                Data = _mapper.Map<IEnumerable<UserDto>>(data)
            };
        }

        public async Task<bool> IsEmailExists(string email)
        {
            return await _unitOfWork.UserRepository.IsEmailExistsAsync(email);
        }

        public async Task<bool> IsIdExists(Guid id)
        {
            return await _unitOfWork.UserRepository.IsIdExistsAsync(id);
        }

        public async Task UpdateRole(string id, string roleName)
        {
            var user = await _unitOfWork.UserRepository.GetAsync(ParseGuidString(id));
            var role = await _unitOfWork.RoleRepository.GetAsync(roleName);
            
            user.Role = role;
            
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}