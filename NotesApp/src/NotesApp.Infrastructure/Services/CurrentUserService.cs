﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using NotesApp.Application.Dto;
using NotesApp.Application.Services;

namespace NotesApp.Infrastructure.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public string? Id { get; set; }
        public string? Nickname { get; set; }
        public string? Role { get; set; }

        private DateTime? _lastUpdateTime;

        private readonly IHttpContextAccessor _contextAccessor;

        public CurrentUserService(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
            _lastUpdateTime = null;
        }

        public async Task RefreshData(bool refreshRole = false)
        {
            if (_contextAccessor.HttpContext?.User.Identity is null ||
                !_contextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                Id = Nickname = Role  = null;
                _lastUpdateTime = null;
                return;
            }

            if (_lastUpdateTime is not null && DateTime.UtcNow.Subtract(_lastUpdateTime.Value) < new TimeSpan(0, 0, 1, 0))
            {
                _contextAccessor.HttpContext.User.AddIdentity(new ClaimsIdentity(new List<Claim>
                {
                    new(ClaimTypes.Role, Role!)
                }));

                return;
            }

            _lastUpdateTime = DateTime.UtcNow;

            Id = _contextAccessor.HttpContext.User.FindFirst(c => c.Type == JwtRegisteredClaimNames.Sub)?.Value;
            Nickname = _contextAccessor.HttpContext.User.FindFirst(c => c.Type == JwtRegisteredClaimNames.UniqueName)
                ?.Value;

            // Role ??= _contextAccessor.HttpContext.User.FindFirst(c => c.Type == ClaimTypes.Role)?.Value;

            var userService = _contextAccessor.HttpContext.RequestServices.GetRequiredService<IUserService>();
            
            Role = (await userService.GetAsync(Id))
                .Role.Name;

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine($"\nNickname: {Nickname}");
            Console.WriteLine($"Role: {Role}");
            Console.WriteLine($"Next role update at: {_lastUpdateTime.Value.AddMinutes(1)}\n");
            Console.ResetColor();
            
            _contextAccessor.HttpContext.User.AddIdentity(new ClaimsIdentity(new List<Claim>
            {
                new(ClaimTypes.Role, Role!)
            }));
        }
    }
}