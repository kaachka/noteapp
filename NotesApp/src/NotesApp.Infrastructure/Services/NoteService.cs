﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using NotesApp.Application.Dto;
using NotesApp.Application.Exceptions;
using NotesApp.Application.Options;
using NotesApp.Application.Persistence;
using NotesApp.Application.Services;
using NotesApp.Application.Validators;
using NotesApp.Domain.Entities;

namespace NotesApp.Infrastructure.Services
{
    public class NoteService : INoteService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        
        private readonly NotesOptions _notesOptions;

        private readonly ICurrentUserService _currentUserService;
        private readonly IRawNoteService _rawNoteService;
        
        private readonly NoteValidator _noteValidator;
        private readonly PaginationValidator _paginationValidator;

        public NoteService(IMapper mapper,
            IUnitOfWork unitOfWork,
            NotesOptions notesOptions,
            ICurrentUserService currentUserService,
            IRawNoteService rawNoteService,
            NoteValidator noteValidator,
            PaginationValidator paginationValidator)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            
            _notesOptions = notesOptions;

            _currentUserService = currentUserService;
            _rawNoteService = rawNoteService;

            _noteValidator = noteValidator;
            _paginationValidator = paginationValidator;
        }

        private async Task<Pagination> ConfirmPagination(Pagination pagination)
        {
            var result = await _paginationValidator.ValidateAsync(pagination);

            return result.IsValid ? pagination : new Pagination();
        }

        private async Task ValidateAsync(Note entity)
        {
            var result = await _noteValidator.ValidateAsync(entity);

            if (!result.IsValid)
                throw new ValidationException(result.Errors);
        }

        public async Task CreateAsync(NoteDto noteDto)
        {
            var rawNote = _mapper.Map<RawNoteDto>(noteDto);

            if (_currentUserService.Id is null)
            {
                rawNote.LifeTime = DateTime.UtcNow.AddHours(_notesOptions.Lifetime);
                
                await _rawNoteService.CreateAsync(rawNote);
                return;
            }

            var userId = new Guid(_currentUserService.Id);
            
            if (noteDto.AccessType == AccessType.Public)
            {   
                rawNote.User = new UserDto
                {
                    Id = userId
                };
                
                await _rawNoteService.CreateAsync(rawNote);
                return;
            }
            
            var note = _mapper.Map<Note>(noteDto);

            await ValidateAsync(note);

            note.UserId = userId;
            await _unitOfWork.NoteRepository.CreateAsync(note);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<NoteDto> GetAsync(Guid id)
        {
            return _mapper.Map<NoteDto>(await _unitOfWork.NoteRepository.GetAsync(id));
        }

        public async Task<NoteDto> GetAsync(string id)
        {
            Guid parsedId;
            
            try
            {
                parsedId = new Guid(id);
            }
            catch
            {
                throw new ValidationException("Incorrect id type.");
            }

            return _mapper.Map<NoteDto>(await _unitOfWork.NoteRepository.GetAsync(parsedId));
        }

        public async Task<PaginationResultDto<NoteDto>> GetAllAsync(Pagination pagination)
        {
            var confirmedPagination = await ConfirmPagination(pagination);

            var data = await _unitOfWork.NoteRepository.GetAllAsync(confirmedPagination);
            
            return new PaginationResultDto<NoteDto>
            {
                PageNumber = confirmedPagination.PageNumber,
                PageSize = confirmedPagination.PageSize,
                Data = _mapper.Map<IEnumerable<NoteDto>>(data)
            };
        }

        public async Task<PaginationResultDto<NoteDto>> GetByAccessTypeAsync(Pagination pagination,
            AccessType accessType = AccessType.Public)
        {
            var confirmedPagination = await ConfirmPagination(pagination);

            var data = await _unitOfWork.NoteRepository.GetByAccessTypeAsync(confirmedPagination, accessType);

            return new PaginationResultDto<NoteDto>
            {
                PageNumber = confirmedPagination.PageNumber,
                PageSize = confirmedPagination.PageSize,
                Data = _mapper.Map<IEnumerable<NoteDto>>(data)
            };
        }

        public async Task<PaginationResultDto<NoteDto>> GetExpiredAsync(Pagination pagination,
            AccessType accessType = AccessType.Public)
        {
            var confirmedPagination = await ConfirmPagination(pagination);

            var data = await _unitOfWork.NoteRepository.GetExpiredAsync(confirmedPagination, accessType);

            return new PaginationResultDto<NoteDto>
            {
                PageNumber = confirmedPagination.PageNumber,
                PageSize = confirmedPagination.PageSize,
                Data = _mapper.Map<IEnumerable<NoteDto>>(data)
            };
        }

        public async Task<PaginationResultDto<NoteDto>> FindAsync(Pagination pagination, string title)
        {
            if (string.IsNullOrWhiteSpace(title))
                throw new ValidationException("Title is required");
            
            var confirmedPagination = await ConfirmPagination(pagination);

            var data = await _unitOfWork.NoteRepository.FindAsync(pagination, title);

            return new PaginationResultDto<NoteDto>
            {
                Data = _mapper.Map<IEnumerable<NoteDto>>(data),
                PageNumber = confirmedPagination.PageNumber,
                PageSize = confirmedPagination.PageSize
            };
        }

        public async Task UpdateAsync(NoteDto noteDto)
        {
            if(!await _unitOfWork.NoteRepository.IsIdExists(noteDto.Id))
                return;
            
            if (noteDto.AccessType == AccessType.Public)
            {   
                var userId = new Guid(_currentUserService.Id);

                var rawNote = _mapper.Map<RawNoteDto>(noteDto);
                
                rawNote.User = new UserDto
                {
                    Id = userId
                };

                await RemoveAsync(noteDto.Id);
                await _rawNoteService.CreateAsync(rawNote);
                return;
            }
            
            var note = _mapper.Map<Note>(noteDto);

            await ValidateAsync(note);

            var updatableNote = await _unitOfWork.NoteRepository.GetAsync(note.Id);

            updatableNote.Title = note.Title;
            updatableNote.Text = note.Text;
            updatableNote.AccessType = note.AccessType;
            updatableNote.LifeTime = note.LifeTime;

            _unitOfWork.NoteRepository.Update(updatableNote);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            await _unitOfWork.NoteRepository.RemoveAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}