﻿using System.Linq;
using NotesApp.Domain.Entities;

namespace NotesApp.Infrastructure.Persistence.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<TEntity> Limit<TEntity>(this IQueryable<TEntity> entities, Pagination pagination)
        {
            return entities.Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize);
        }
    }
}