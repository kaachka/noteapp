﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NotesApp.Application.Exceptions;
using NotesApp.Application.Persistence.Repositories;
using NotesApp.Domain.Entities;
using NotesApp.Infrastructure.Persistence.Database;
using NotesApp.Infrastructure.Persistence.Extensions;

namespace NotesApp.Infrastructure.Persistence.Repositories
{
    public class NoteRepository : Repository<Note, Guid>, INoteRepository
    {
        private readonly DbSet<Note> _notes;
        
        public NoteRepository(ApplicationDbContext context) : base(context)
        {
            _notes = context.Notes;
        }

        public override async Task<Note> GetAsync(Guid id)
        {
            return await _notes.Include(n => n.User).FirstOrDefaultAsync(n => n.Id == id)  ?? throw new NotFoundException(nameof(Note), id);
        }

        public override async Task<IEnumerable<Note>> GetAllAsync(Pagination pagination)
        {
            return await _notes.Include(n => n.User).Where(n => n.AccessType == AccessType.Public).OrderByDescending(n => n.Created).Limit(pagination)
                .ToListAsync();
        }

        public async Task<bool> IsIdExists(Guid id)
        {
            return await _notes.AnyAsync(n => n.Id == id);
        }

        public async Task<IEnumerable<Note>> GetByAccessTypeAsync(Pagination pagination = null, AccessType accessType = AccessType.Public)
        {
            return await _notes.Include(n => n.User).Where(n => n.AccessType == accessType).OrderByDescending(n => n.Created)
                .Limit(pagination).ToListAsync();
        }

        public async Task<IEnumerable<Note>> GetExpiredAsync(Pagination pagination = null, AccessType accessType = AccessType.Public)
        {
            return await _notes.Include(n => n.User)
                .Where(n => n.AccessType == accessType && n.LifeTime >= DateTime.UtcNow).OrderByDescending(n => n.Created)
                .Limit(pagination).ToListAsync();
        }

        public async Task<IEnumerable<Note>> FindAsync(Pagination pagination, string title)
        {
            return await _notes.Include(n => n.User)
                .Where(n => n.AccessType == AccessType.Public && EF.Functions.Like(n.Title, $"{title}%"))
                .OrderByDescending(n => n.Created)
                .Limit(pagination).ToListAsync();
        }
    }
}