﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NotesApp.Application.Exceptions;
using NotesApp.Application.Persistence.Repositories;
using NotesApp.Domain.Entities;
using NotesApp.Infrastructure.Persistence.Database;
using NotesApp.Infrastructure.Persistence.Extensions;

namespace NotesApp.Infrastructure.Persistence.Repositories
{
    public class RawNoteRepository : Repository<RawNote, Guid>, IRawNoteRepository
    {
        private readonly DbSet<RawNote> _rawNotes;
        
        public RawNoteRepository(ApplicationDbContext context) : base(context)
        {
            _rawNotes = context.RawNotes;
        }

        public override async Task<RawNote> GetAsync(Guid id)
        {
            return await _rawNotes.Include(n => n.User).FirstOrDefaultAsync(n => n.Id == id)  ?? throw new NotFoundException(nameof(RawNote), id);
        }

        public override async Task<IEnumerable<RawNote>> GetAllAsync(Pagination pagination)
        {
            return await _rawNotes.Include(n => n.User).OrderBy(n => n.Created).Limit(pagination).ToListAsync();
        }
    }
}