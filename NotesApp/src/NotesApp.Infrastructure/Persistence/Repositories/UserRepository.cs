﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NotesApp.Application.Exceptions;
using NotesApp.Application.Persistence.Repositories;
using NotesApp.Domain.Entities;
using NotesApp.Infrastructure.Persistence.Database;
using NotesApp.Infrastructure.Persistence.Extensions;

namespace NotesApp.Infrastructure.Persistence.Repositories
{
    public class UserRepository : Repository<User, Guid>, IUserRepository
    {
        private readonly DbSet<User> _users;
        
        public UserRepository(ApplicationDbContext context) : base(context)
        {
            _users = context.Users;
        }

        public async Task<bool> IsEmailExistsAsync(string email)
        {
            return await _users.AnyAsync(u => u.Email == email);
        }

        public async Task<bool> IsIdExistsAsync(Guid id)
        {
            return await _users.AnyAsync(u => u.Id == id);
        }

        public async Task<User> GetAsync(Guid id, bool fullInfo = false)
        {
            var query = _users.Include(u => u.Role);

            if (fullInfo)
                return await query.Include(u => u.Notes).FirstOrDefaultAsync(u => u.Id == id) ?? throw new NotFoundException(nameof(User), id);

            return await query.FirstOrDefaultAsync(u => u.Id == id) ?? throw new NotFoundException(nameof(User), id);
        }

        public async Task<IEnumerable<User>> GetAllAsync(Pagination pagination, bool fullInfo = false)
        {
            var query = _users.Include(u => u.Role);

            if (fullInfo)
                return await query.Include(u => u.Notes).OrderBy(u => u.Role.Name).Limit(pagination).ToListAsync();

            return await query.OrderBy(u => u.Role.Name).Limit(pagination).ToListAsync();
        }
    }
}