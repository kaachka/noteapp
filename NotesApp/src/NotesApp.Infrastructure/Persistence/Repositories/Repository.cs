﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NotesApp.Application.Exceptions;
using NotesApp.Application.Persistence.Repositories;
using NotesApp.Domain.Entities;
using NotesApp.Infrastructure.Persistence.Extensions;

namespace NotesApp.Infrastructure.Persistence.Repositories
{
    public abstract class Repository<TEntity, TId> : IRepository<TEntity, TId> 
        where TEntity : class
        where TId : struct
    {
        private readonly DbSet<TEntity> _entities;

        protected Repository(DbContext context)
        {
            _entities = context.Set<TEntity>();
        }

        public async Task CreateAsync(TEntity entity)
        {
            await _entities.AddAsync(entity);
        }

        public virtual async Task<TEntity> GetAsync(TId id)
        {
            return await _entities.FindAsync(id) ?? throw new NotFoundException(typeof(TEntity).Name, id);
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync(Pagination pagination)
        {
            return await _entities.Limit(pagination).ToListAsync();
        }

        public void Update(TEntity entity)
        {
            _entities.Update(entity);
        }

        public async Task RemoveAsync(TId id)
        {
            var entity = await GetAsync(id);

            _entities.Remove(entity);
        }
    }
}