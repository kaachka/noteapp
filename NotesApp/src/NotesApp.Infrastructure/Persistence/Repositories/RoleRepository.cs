﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NotesApp.Application.Exceptions;
using NotesApp.Application.Persistence.Repositories;
using NotesApp.Domain.Entities;
using NotesApp.Infrastructure.Persistence.Database;

namespace NotesApp.Infrastructure.Persistence.Repositories
{
    public class RoleRepository : Repository<Role, Guid>, IRoleRepository
    {
        private readonly DbSet<Role> _roles;

        public RoleRepository(ApplicationDbContext context) : base(context)
        {
            _roles = context.Roles;
        }

        public async Task<Role> GetAsync(string name)
        {
            return await _roles.FirstOrDefaultAsync(r => r.Name == name) ?? throw new NotFoundException(nameof(Role), name, "name");
        }
    }
}