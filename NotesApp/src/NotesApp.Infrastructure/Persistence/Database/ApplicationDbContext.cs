﻿using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NotesApp.Application.Services;
using NotesApp.Domain.Entities;

namespace NotesApp.Infrastructure.Persistence.Database
{
    public sealed class ApplicationDbContext : DbContext
    {
        private readonly ICurrentUserService _currentUserService;

        public readonly DbSet<Role> Roles;
        public readonly DbSet<User> Users;
        public readonly DbSet<RawNote> RawNotes;
        public readonly DbSet<Note> Notes;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, ICurrentUserService currentUserService) : base(options)
        {
            _currentUserService = currentUserService;

            Roles = Set<Role>();
            Users = Set<User>();
            RawNotes = Set<RawNote>();
            Notes = Set<Note>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            
            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.Created = DateTime.UtcNow;
                        entry.Entity.CreatedBy = string.IsNullOrWhiteSpace(entry.Entity.CreatedBy) ? _currentUserService.Nickname ?? "Anonymous" : entry.Entity.CreatedBy;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModified = DateTime.UtcNow;
                        entry.Entity.LastModifiedBy = _currentUserService.Id;
                        break;
                }
            }
            
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}