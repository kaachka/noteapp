﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NotesApp.Domain.Entities;

namespace NotesApp.Infrastructure.Persistence.Configurations
{
    public class UserConfigurations : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(u => u.FirstName).IsRequired().HasMaxLength(48);
            builder.Property(u => u.LastName).IsRequired().HasMaxLength(48);
            builder.Property(u => u.Nickname).IsRequired().HasMaxLength(32);
            builder.Property(u => u.Email).IsRequired().HasMaxLength(128);

            builder.
                HasOne(u => u.Role).
                WithMany(r => r.Users).
                HasForeignKey(u => u.RoleId).
                OnDelete(DeleteBehavior.Restrict);

            builder.
                HasMany(u => u.RawNotes).
                WithOne(n => n.User).
                HasForeignKey(n => n.UserId);

            builder.
                HasMany(u => u.Notes).
                WithOne(n => n.User).
                HasForeignKey(n => n.UserId);
        }
    }
}