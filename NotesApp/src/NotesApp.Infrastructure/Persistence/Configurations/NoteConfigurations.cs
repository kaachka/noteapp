﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NotesApp.Domain.Entities;

namespace NotesApp.Infrastructure.Persistence.Configurations
{
    public class NoteConfigurations : IEntityTypeConfiguration<Note>
    {
        public void Configure(EntityTypeBuilder<Note> builder)
        {
            // builder.HasOne(n => n.User).WithMany(u => u.Notes).HasForeignKey(n => n.UserId);
    
            builder.Property(n => n.Title).IsRequired().HasMaxLength(80);
            builder.Property(n => n.Description).HasMaxLength(128);
            builder.Property(n => n.Text).IsRequired();
            builder.Property(n => n.AccessType).IsRequired();
            
            builder.Property(n => n.Created).IsRequired();
            builder.Property(n => n.CreatedBy).IsRequired().HasMaxLength(256);
    
            builder.Property(n => n.LastModifiedBy).HasMaxLength(256);
        }
    }
}