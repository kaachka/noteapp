﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NotesApp.Domain.Entities;

namespace NotesApp.Infrastructure.Persistence.Configurations
{
    public class RawNoteConfigurations : IEntityTypeConfiguration<RawNote>
    {
        public void Configure(EntityTypeBuilder<RawNote> builder)
        {
            // builder.HasOne(n => n.User).WithMany(u => u.RawNotes).HasForeignKey(n => n.UserId);
    
            builder.Property(n => n.Title).IsRequired().HasMaxLength(50);
            builder.Property(n => n.Description).HasMaxLength(80);
            builder.Property(n => n.Text).IsRequired();
            
            builder.Property(n => n.Created).IsRequired();
            builder.Property(n => n.CreatedBy).IsRequired().HasMaxLength(128);
    
            builder.Property(n => n.LastModifiedBy).HasMaxLength(128);
        }
    }
}