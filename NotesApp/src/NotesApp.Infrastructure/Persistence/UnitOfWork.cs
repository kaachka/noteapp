﻿using System.Threading.Tasks;
using NotesApp.Application.Persistence;
using NotesApp.Application.Persistence.Repositories;
using NotesApp.Infrastructure.Persistence.Database;
using NotesApp.Infrastructure.Persistence.Repositories;

namespace NotesApp.Infrastructure.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        public IRoleRepository RoleRepository { get; }
        public IUserRepository UserRepository { get; }
        public IRawNoteRepository RawNoteRepository { get; }
        public INoteRepository NoteRepository { get; }

        private readonly ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            RoleRepository = new RoleRepository(context);
            UserRepository = new UserRepository(context);
            RawNoteRepository = new RawNoteRepository(context);
            NoteRepository = new NoteRepository(context);
        }
        
        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}